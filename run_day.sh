#!/usr/bin/env bash

if [[ "$#" < 2 ]]; then
    echo "Insufficient arguments"
    echo ""
    echo "./run_day.sh $FOLDER $DAY" 
    exit 1
fi

if [[ "$1" == "py" ]]; then
    COMMAND="python3 py/day$2.py"
elif [[ "$1" == "js" ]]; then
    COMMAND="deno run js/day$2.js"
else
    echo "Unknown folder"
    exit 1
fi

if [[ ! -f "inputs/day$2.txt" ]]; then
    /usr/bin/time -f "time ${COMMAND} (%es)" ${COMMAND}
else
    /usr/bin/time -f "time ${COMMAND} < inputs/day$2.txt (%es)" ${COMMAND} < "inputs/day$2.txt"
fi
