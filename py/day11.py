#!/usr/bin/env python
import sys
import itertools

def day11():
    text = []
    text2 = []
    for line in sys.stdin:
        text.append(list(line.strip()))
        text2.append(list(line.strip()))

    filled = seats_filled(text) 
    text = cycle(text)
    while filled != seats_filled(text):
        filled = seats_filled(text)
        text = cycle(text)
    print(f"Pt1: {filled}")

    filled = seats_filled(text2)
    text2 = cycle_extended(text2)
    while filled != seats_filled(text2):
        filled = seats_filled(text2)
        text2 = cycle_extended(text2)
    print(f"Pt2: {filled}")
    

def cycle_extended(text):
    cycled = []
    #row limit, column limit
    rlim = len(text) - 1
    clim = len(text[0]) - 1
    for row in range(rlim+1):
        cycled_row = []
        for col in range(clim+1):
            if text[row][col] == ".":
                cycled_row.append(".")
                continue
            score = 0
            score += in_sight(text, row-1, col-1, -1, -1)
            score += in_sight(text, row-1, col, -1, 0)
            score += in_sight(text, row, col-1, 0, -1)
            score += in_sight(text, row-1, col+1, -1, +1)
            score += in_sight(text, row, col+1, 0, +1)
            score += in_sight(text, row+1, col-1, +1, -1)
            score += in_sight(text, row+1, col, +1, 0)
            score += in_sight(text, row+1, col+1, +1, +1)
            if text[row][col] == "#" and score > 4:
                cycled_row.append("L")
                continue
            if text[row][col] == "L" and score == 0:
                cycled_row.append("#")
                continue
            cycled_row.append(text[row][col])
        cycled.append(cycled_row)
    return cycled

def in_sight(text, row, col, drow, dcol):
    if row < 0 or col < 0 or row > len(text)-1 or col > len(text[0])-1:
        return False
    if text[row][col] == "L":
        return False
    if text[row][col] == "#":
        return True
    return in_sight(text, row + drow, col + dcol, drow, dcol)

def cycle(text):
    cycled = []
    #row limit, column limit
    rlim = len(text) - 1
    clim = len(text[0]) - 1
    for row in range(rlim+1):
        cycled_row = []
        for col in range(clim+1):
            if text[row][col] == ".":
                cycled_row.append(".")
                continue

            score = 0
            score += (row > 0 and col > 0 and text[row-1][col-1] == "#")
            score += (row > 0 and text[row-1][col] == "#")
            score += (col > 0 and text[row][col-1] == "#")
            score += (row > 0 and col < clim and text[row-1][col+1] == "#")
            score += (col < clim and text[row][col+1] == "#")
            score += (row < rlim and col > 0 and text[row+1][col-1] == "#")
            score += (row < rlim and text[row+1][col] == "#")
            score += (row < rlim and col < clim and text[row+1][col+1] == "#")

            if text[row][col] == "L" and score == 0:
                cycled_row.append("#")
                continue
            if text[row][col] == "#" and score > 3:
                cycled_row.append("L")
                continue
            cycled_row.append(text[row][col])
        cycled.append(cycled_row)
    return cycled

def seats_filled(text):
    return sum([row.count("#") for row in text])
    
if __name__ == "__main__":
    day11()
