#!/usr/bin/env python

def day15():
    input_text = "20,9,11,0,1,2"
    #input_text = "3,1,2" #Sample input

    puzzle = {} 
    turn = 0
    last = 0
    for idx, val in enumerate(input_text.split(",")):
        turn = idx
        last = int(val)
        puzzle[int(val)] = idx 

    while True:
        #0 indexed, so turn 2019 has 2020th value
        if turn == (2020 - 1):
            print(f"Pt1: {last}")
        elif turn == (30000000 - 1):
            print(f"Pt2: {last}")
            break

        #Determine next value
        if last not in puzzle:
            val = 0 
        else:
            val = turn - puzzle[last]

        #Set previous value
        puzzle[last] = turn

        #Next value is now previous one
        last = val
        turn += 1

if __name__ == "__main__":
    day15()
