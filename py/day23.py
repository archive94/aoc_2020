#!/usr/bin/env python

def day23():
    input_text = "135468729"
    #input_text = "389125467" #Sample input
    cup_map = create_puzzle(input_text) 
    current = int(input_text[0])
    for _ in range(100):
        current = move(cup_map, current, 9)
    print(f"Pt1: {puzzle_string(cup_map)}")

    cup_map = create_puzzle(input_text, True) 
    current = int(input_text[0])
    for _ in range(10000000):
        current = move(cup_map, current, 1000000)
    puzzle_product = cup_map[1]["next"]["value"] 
    puzzle_product *= cup_map[1]["next"]["next"]["value"]
    print(f"Pt2: {puzzle_product}")

def move(cup_map, current, max_value):
    selection = cup_map[current]["next"]
    cup_map[current]["next"] = selection["next"]["next"]["next"]
    selection["next"]["next"]["next"] = None
    selection_values = [
        selection["value"],
        selection["next"]["value"],
        selection["next"]["next"]["value"],
    ]
    value = current - 1
    while value in selection_values or value == 0:
        value -= 1
        if value <= 0:
            value = max_value
    insert = cup_map[value]
    selection["next"]["next"]["next"] = insert["next"]
    insert["next"] = selection
    return cup_map[current]["next"]["value"]

def puzzle_string(cup_map):
    puzzle_string = ""
    current = cup_map[1]["next"]
    while current["value"] != 1:
        puzzle_string += str(current["value"])
        current = current["next"]
    return puzzle_string

def create_puzzle(input_text, p2=False):
    order = [] 
    cup_map = {}
    for value in input_text:
        order.append(int(value))
        cup_map[int(value)] = {"value": int(value)}
    if p2:
        for value in range(10, 1000001):
            order.append(value)
            cup_map[value] = {"value": value}
    for idx in range(len(order)):
        next_idx = order[(idx+1)%len(order)]
        next_cup = cup_map[next_idx]
        cup_map[order[idx]]["next"] = next_cup 
    return cup_map

if __name__ == "__main__":
    day23()
