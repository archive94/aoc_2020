#!/usr/bin/env python
import sys

def day01():
    expense_report = [int(line) for line in sys.stdin]
    expense_report.sort()

    sum_pair = find_sum_pair(2020, expense_report) 
    if sum_pair[0] == None:
        print("No sum pair found")
    else:
        pair_product = sum_pair[0] * sum_pair[1]
        print(f"Pt1: {sum_pair[0]*sum_pair[1]}")

    sum_trio = find_sum_trio(2020, expense_report)
    if sum_trio[0] == None:
        print("No sum trio found")
    else:
        trio_product = sum_trio[0] * sum_trio[1] * sum_trio[2]
        print(f"Pt2: {trio_product}")

def find_sum_pair(total, values):
    for idx in range(len(values)):
        complement = total - values[idx]
        if complement in values and values.index(complement) != idx:
            return [values[idx], complement]
    return [None, None]

def find_sum_trio(total, values):
    for idx_1 in range(len(values)):
        for idx_2 in range(idx_1, len(values)):
            if idx_1 == idx_2:
                continue
            complement = total - values[idx_1] - values[idx_2]
            if complement in values and values.index(complement) > idx_2:
                return [values[idx_1], values[idx_2], complement]
    return [None, None, None]
    

if __name__ == "__main__":
    day01()
