#!/usr/bin/env python
import sys

def day12():
    directions = [line.strip() for line in sys.stdin]
    location = 0 + 0j
    ship_faces = 1
    for idx in range(len(directions)):
        direction = directions[idx][0]
        distance = int(directions[idx][1:])
        if direction == "L":
            ship_faces *= (1j)**(distance/90)
        elif direction == "R":
            ship_faces *= (-1j)**(distance/90)
        elif direction == "F":
            location += ship_faces*distance
        elif direction == "N":
            location += 1j*distance
        elif direction == "S":
            location += -1j*distance
        elif direction == "E":
            location += 1*distance
        elif direction == "W":
            location += -1*distance        
    pt1 = abs(int(location.real)) + abs(int(location.imag))
    print(f"Pt1: {pt1}")

    location = 0 + 0j
    waypoint_offset = 10 + 1j
    for idx in range(len(directions)):
        direction = directions[idx][0]
        distance = int(directions[idx][1:])
        if direction == "L":
            waypoint_offset *= (1j)**(distance/90)
        elif direction == "R":
            waypoint_offset *= (-1j)**(distance/90)
        elif direction == "F":
            location += distance * waypoint_offset
        elif direction == "N":
            waypoint_offset += 1j*distance
        elif direction == "S":
            waypoint_offset += -1j*distance
        elif direction == "E":
            waypoint_offset += 1*distance
        elif direction == "W":
            waypoint_offset += -1*distance
    pt2 = abs(int(location.real)) + abs(int(location.imag))
    print(f"Pt2: {pt2}")

if __name__ == "__main__":
    day12()
