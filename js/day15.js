function main() {
    let input_text = "20,9,11,0,1,2";
    //input_text = "0,3,6"; //Sample input

    let puzzle = new Map();
    let turn = 0;
    let last = 0;
    let input = input_text.split(",");
    for (let idx = 0; idx < input.length; idx++) {
        turn = idx;
        last = Number(input[idx]);
        puzzle.set(last, turn);
    }

    let val=null;
    while (true) {
        //0 indexed, so turn 2019 has 2020th value
        if (turn == (2020 - 1)) {
            console.log(`Pt1: ${last}`);
        } else if (turn == (30000000 - 1)) {
            console.log(`Pt2: ${last}`);
            break;
        }

        //Determine next value
        if (!puzzle.has(last)) {
            val = 0;
        } else {
            val = turn - puzzle.get(last);
        }

        //Set previous value
        val = val;
        puzzle.set(last, turn);

        //Next value is now previous one
        last = val;
        turn++;
    }
}

main()
