function main() {
    const decoder = new TextDecoder("utf-8");
    const stdinData = Deno.readAllSync(Deno.stdin);
    const inputText = decoder.decode(stdinData);

    let anyYes = [];
    let allYes = [];
    for (let group of inputText.split("\n\n")) {
        anyYes.push(new Set(group.replaceAll("\n", "")));
        let groupResponses = [];
        for (let response of group.split("\n")) {
            if (response == "") {
                continue;
            }
            groupResponses.push(response.split(""));
        }
        allYes.push(intersection(groupResponses));
    }
    let totalAnyYes = 0;
    let totalAllYes = 0;
    for (let idx = 0; idx < anyYes.length; idx++) {
        totalAnyYes += anyYes[idx].size;
        totalAllYes += allYes[idx].size;
    }
    console.log(`Pt1: ${totalAnyYes}`)
    console.log(`Pt1: ${totalAllYes}`)
}

function intersection(groupData) {
    let intersect = groupData[0];
    for (let idx = 1; idx < groupData.length; idx++) {
        intersect = groupData[idx].filter(val => intersect.includes(val));
    }
    //console.log(intersect);
    return new Set(intersect);
}

main()
/*
#!/usr/bin/env python
import fileinput

def day06():
    input_text = "".join([line for line in fileinput.input()])
    any_yes = []
    all_yes = []
    for group in input_text.split("\n\n"):
        any_yes.push(new Set(group.replaceAll("\n", "")));
        for response in group.splitlines():
            group_sets.append(set(response))
        any_yes.append(set.union(*group_sets))
        all_yes.append(set.intersection(*group_sets))
    pt1 = sum([len(ans) for ans in any_yes])
    pt2 = sum([len(ans) for ans in all_yes])
    print(f"Pt1: {pt1}")
    print(f"Pt2: {pt2}")

if __name__ == "__main__":
    day06()
*/
