async function main() {
    const decoder = new TextDecoder("utf-8");
    const stdinData = await Deno.readAll(Deno.stdin);
    const inputText = decoder.decode(stdinData);

    let program = inputText.split("\n");
    if (program[program.length - 1] == ""){
        program.pop();
    }

    let pt1 = await runProgram(program);
    let pt2;
    logPt1(pt1);
    let testProgram;
    for (let idx = 0; idx < program.length; idx++) {
        testProgram = [...program];
        if (program[idx].includes("nop")) {
            testProgram[idx] = "jmp " + program[idx].substring(4);
            pt2 = await runProgram(testProgram)
            logPt2(pt2);
        } else if (program[idx].includes("jmp")) {
            testProgram[idx] = "nop " + program[idx].substring(4);
            pt2 = await runProgram(testProgram)
            logPt2(pt2);
        }
    }
}

async function runProgram(program) {
    let instructionsRun = [];
    let idx = 0;
    let programValue = 0;
    while (idx < program.length) {
        if (instructionsRun.includes(idx)) {    
            return [programValue, idx];
        }
        instructionsRun.push(idx);
        let [instr, value] = program[idx].split(" ");
        value = Number(value);
        if (instr == "acc") {
            programValue += value;
        } else if (instr == "jmp") {
            idx += value;
            continue; //Skip index increment
        }
        idx += 1; //Post "acc". Also "nop"
    }
    return [programValue, null];
}

async function logPt1(programOutput) {
    console.log(`Pt1: ${programOutput[0]}`);
}

async function logPt2(programOutput) {
    //console.log(programOutput);
    if (programOutput[1] != null) {
        return;
    }
    console.log(`Pt2: ${programOutput[0]}`);
}

main()
