function main() {
    const decoder = new TextDecoder("utf-8");
    const stdinData = Deno.readAllSync(Deno.stdin);
    const inputText = decoder.decode(stdinData);

    let seat_ids = [];
    let id;
    for (let seat of inputText.split("\n")) {
        id = parseInt(seat
            .replaceAll("F", "0")
            .replaceAll("B", "1")
            .replaceAll("L", "0")
            .replaceAll("R", "1"), 2);
        if (isNaN(id)) { continue; }
        seat_ids.push(id);
    }
    seat_ids.sort((a, b) => a - b);
    console.log(`Pt1: ${seat_ids[seat_ids.length-1]}`);

    for (let idx = 25; idx < seat_ids.length; idx++) {
        if (seat_ids[idx] == (seat_ids[idx-1] + 1)) { continue; }
        console.log(`Pt2: ${seat_ids[idx]-1}`);
        return;
    }
}

main()
