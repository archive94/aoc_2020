async function main() {
    const decoder = new TextDecoder("utf-8");
    const stdinData = await Deno.readAll(Deno.stdin);
    const inputText = decoder.decode(stdinData);

    let expenseReport = [];
    for (let line of inputText.split("\n")) {
        if (line == "") { continue; }
        expenseReport.push(Number(line));
    }
    //console.log(expense_report);

    findSumPair(2020, expenseReport).then(displaySumPair);
    findSumTrio(2020, expenseReport).then(displaySumTrio);
}

async function displaySumPair(sumPair) {
    if (!sumPair[0]) {
        console.log("No sum pair found");
    } else {
        let pairProduct = sumPair[0] * sumPair[1];
        console.log(`Pt1: ${pairProduct}`);
    }
}

async function displaySumTrio(sumTrio) {
    if (!sumTrio[0]) {
        console.log("No sum trio found");
    } else {
        let trioProduct = sumTrio[0] * sumTrio[1] * sumTrio[2];
        console.log(`Pt2: ${trioProduct}`);
    }
}

async function findSumPair(total, values) {
    let complement;
    let complementIdx;
    for (let idx=0; idx < values.length; idx++) {
        complement = total - values[idx];
        complementIdx = values.indexOf(complement);
        if (complementIdx != -1 && complementIdx != idx) {
            return [values[idx], complement];
        }
    }
    return [null, null];
}

async function findSumTrio(total, values) {
    let complement;
    let complementIdx;
    for (let idx1 = 0; idx1 < values.length; idx1++) {
        for (let idx2 = idx1; idx2 < values.length; idx2++) {
            complement = total - values[idx1] - values[idx2];
            complementIdx = values.indexOf(complement);
            if (complementIdx != -1 && complementIdx > idx2) {
                return [values[idx1], values[idx2], complement];
            }
        }
    }
    return [null, null, null];
}

main()
